# This repo hosts the pipeline-synthesis benchmarks used by [Auto-Pipeline](https://arxiv.org/abs/2106.13861). 

We created two benchmarks while working on Auto-Pipeline, in order to evaluate the effectiveness and efficiency of pipeline synthesis, using the "by-target" paradigm we propose in the paper: 
  - **GitHub**: this benchmark has 700 real data pipelines, which are extracted from executing Jupter notebooks that are authored by human experts and hosted on Github. We randomly sample 100 pipelines with {1, 2, 3, 4, 5, [6-8], 9+} steps each, for a total of 700 pipelines. These are our ground-truth pipeline for synthesis.
  - **Commercial**: Since there are many commercial systems (e.g., data prep and ETL vendors) that also help users build complex data pipelines, we create a second benchmark, using pipelines sampled from  4 leading vendors (Alteryx, SQL Server Integration Services, Microsoft Power Query, Microsoft Azure Data Factory), and manually collect 16 demo pipelines from official tutorials of these vendors, as ground-truth pipelines for synthesis. 


## Folder structure
Each sub-folder corresponds to one pipeline test case, with the following data files:
- a list of "training data"
  - training_0.csv, training_1.csv, ...
- a list of "test data"
  - test_0.csv. test_1.csv, ...
- target schema, which is generated from running the original ground-truth pipeline
  - target.csv

## Evaluation process
The following figure illustrates our evaluation process. Please refer to our Auto-Pipeline paper for details on how we evaluate whether a synthesized pipeline is semantically identical to the original ground-truth pipeline authored by humans.
![Pipeline](eval-data-release.png)    

## How to adjust to Query-By-Example
  - download the code of QUERY-BY-EXAMPLE
    ```
    git clone https://github.com/Mestway/Scythe.git
    ```
    the jar file is located under /Scythe/out/artifacts/Scythe/
  - generate one file including both input and target schema data
    ```
    $python  convert2querybyexample.py path_to_pipeline target_filepath
    ```
- run the script
  ```
  $java -jar Scythe.jar target_filepath -aggr
  ```

## How to adjust to Auto-Pandas
 - download the code of QUERY-BY-EXAMPLE
    ```
    git clone https://github.com/rbavishi/autopandas
    ```
  - generate one file of benchmark format
    ```
    $python  convert2autopandas.py path_to_pipeline target_filepath
    ```

- run the script
    ```
    $TF_CPP_MIN_LOG_LEVEL=3 /usr/local/bin/autopandas_v2 evaluate synthesis "PipelineAutoBenchmark" model_pandas_generators model_pandas_functions pandas_synthesis_results.csv --top-k-args 5 --use-old-featurization --timeout 360  --length 1 --start 55 --end 80 --load-models-on-demand```

## Licenses
These datasets were compiled from sources that are publicly available. Specifically, data from [these pipelines](https://gitlab.com/jwjwyoung/autopipeline-benchmarks/-/blob/main/licenses/license-github-cuda.csv) is released under the [C-UDA license](https://github.com/microsoft/Computational-Use-of-Data-Agreement) with attributions [here](https://gitlab.com/jwjwyoung/autopipeline-benchmarks/-/blob/main/licenses/license-github-attribution.csv); while data from [these pipelines](https://gitlab.com/jwjwyoung/autopipeline-benchmarks/-/blob/main/licenses/license-kaggle-cc-by.csv) is released under the [CC-BY-4.0 license](https://creativecommons.org/licenses/by/4.0/) with attributions [here](https://gitlab.com/jwjwyoung/autopipeline-benchmarks/-/blob/main/licenses/license-kaggle-attribution.csv).
