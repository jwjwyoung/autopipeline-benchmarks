import sys
import os
template = """
from io import StringIO

import pandas as pd
import numpy as np
from autopandas_v2.evaluation.benchmarks.base import Benchmark
import sys
class PipelineAutoBenchmark:
    class {}(Benchmark):
        def __init__(self):
            super().__init__()
            {}
            {}
            self.inputs = {}
            self.output = {}
            self.funcs = []
            self.seqs = [[0]]
"""


def main():
    if len(sys.argv) < 3:
        print("ERROR ARGUMENT LENGTH")
    folder = sys.argv[1]
    target = sys.argv[2]
    s0 = folder.split("/")[-1].upper()
    if os.path.exists(folder):
        s1 = "folder = \"{}\"".format(folder)
        s2 = "files = os.listdir(folder)"
        s3 = """[pd.read_csv(folder + "/" + f) for f in files if "test" in f]"""
        s4 = """pd.read_csv(folder + "/target.csv")"""
        result = template.format(s0, s1, s2, s3, s4)
        print(result)
        
if __name__ == "__main__":
    main()