import sys
import os
import pandas as pd
con = """
# constraint
{
    "constants": [], 
    "aggregation_functions": ['count', "min","max","average"]
}
"""


def df2sql(df):
    # csv = df.sample(min(10,len(df))).to_csv(index=False,sep='|')
    csv = df.to_csv(index=False, sep="|")
    cs = csv.split("\n")
    result = "| {} |\n".format(cs[0])
    result += "|------------------|\n"
    for c in cs[1:]:
        if len(c) > 0:
            result += "| {} |\n".format(c)
    return result


def create_example2(input_dfs, output_df, filename):
    f = open(filename, "w")
    for i in input_dfs:
        f.write("# input\n\n")
        f.write(df2sql(i))
        f.write("\n")

    f.write("\n")

    f.write("# output\n\n")

    f.write(df2sql(output_df))

    f.write(con)
    f.close()




def main():
    if len(sys.argv) < 3:
        print("ERROR ARGUMENT LENGTH")
    folder = sys.argv[1]
    target = sys.argv[2]
    if os.path.exists(folder):
        files = os.listdir(folder)
        input_dfs = [pd.read_csv(folder + "/" + f) for f in files if "test" in f]
        output_df = pd.read_csv(folder + "/target.csv")
        create_example2(input_dfs, output_df, target)
    
if __name__ == "__main__":
    main()